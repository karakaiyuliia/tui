/*
file:		OuterManager.cpp
purpose:	Outer Facade class implementation
author:		Yuliia Karakai
date:		2/03/2019
*/

#include "../tuilib-dist/include/OuterManager.hpp"

/** @fn HANDLE GetInputHandle()
	@brief wraper around function that retrieves a handle to the specified standard device.
	@return HANDLE
*/
OuterManager::_handler OuterManager::GetInputHandle() {
	auto hStdin = InnerManager::GetInputHandle();

	return hStdin;
}

/** @fn HANDLE GetOutputHandle()
	@brief wraper around function that retrieves a handle to the specified standard device.
	@return HANDLE
*/
OuterManager::_handler OuterManager::GetOutputHandle() {
	auto hStdout = InnerManager::GetOutputHandle();

	return hStdout;
}

/** @fn void CtrlHandler(DWORD ctrlType)
	@brief handles Ctrl C event.
	@return void
*/
void OuterManager::CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		exit(EXIT_SUCCESS);
	}
}

/** @fn void CtrlHandler(DWORD ctrlType)
	@brief installs a control handler to trap ^C.
	@return void
*/
void OuterManager::SetCtrlHandler() {
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)OuterManager::CtrlHandler, TRUE)) {
		cerr << "ERROR: failed to install control handler." << endl;
		exit(EXIT_FAILURE);
	}
}

/** @fn int CtrlHandler(DWORD ctrlType)
	@brief wrapper around function that returns ascii int representation.
	@return int
*/
int OuterManager::GetKeyAscii(_key_ev const& ker) {
	return InnerManager::GetKeyAscii(ker);
}

/** @fn unsigned long GetConsoleSize()
	@brief wrapper around function that returns size of the console.
	@return unsigned long
*/
unsigned long OuterManager::GetConsoleSize() {
	return InnerManager::GetConsoleSize(hConsoleOutput);
}


/** @fn void FillConsoleOutput(unsigned short color)
	@brief wrapper around function that fills buffer cells with color.
	@return void
*/
void OuterManager::FillConsoleOutput(unsigned short color, short x, short y, unsigned long consoleSize) {
	InnerManager::FillConsoleOutput(hConsoleOutput, color, x, y, consoleSize);
}

/** @fn void ConfigureConsoleMode(_handler hStdin)
	@brief wrapper around function that configures console.
	@return void
*/
void OuterManager::ConfigureConsoleMode(_handler hStdin) {
	InnerManager::ConfigureConsoleMode(hStdin);
}

/** @fn void GetBuffer()
	@brief wrapper around function that returns current buffer.
	@return void
*/
void OuterManager::GetBuffer() {
	InnerManager::GetBuffer(hConsoleOutput);
}

/** @fn COORD GetMousePos(MOUSE_EVENT_RECORD const& mer)
	@brief wrapper around function that returns mouse position.
	@return COORD
*/
OuterManager::_coordinate OuterManager::GetMousePos(_mouse_ev const& mer) {
	auto bufferLoc = InnerManager::GetMousePos(mer);

	return bufferLoc;
}

/** @fn unsigned long GetBtnState(MOUSE_EVENT_RECORD const& mer)
	@brief wrapper around function that returns mouse buttons state.
	@return unsigned long
*/
unsigned long OuterManager::GetBtnState(_mouse_ev const& mer) {
	auto mask = InnerManager::GetBtnState(mer);

	return mask;
}

/** @fn void WriteToConsole(string s, vector<WORD> attr, COORD bufferLoc)
	@brief wrapper around function that writes to the console.
	@return void
*/
void OuterManager::WriteToConsole(string s, vector<WORD> attr, COORD bufferLoc) {
	InnerManager::WriteToConsole(hConsoleOutput, s, attr, bufferLoc);
}

/** @fn void SetConsoleBackgroundColor(WORD color)
	@brief sets console background to specified color.
	@return void
*/
void OuterManager::SetConsoleBackgroundColor(WORD color) {
	_handler hOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFOEX cbi;
	cbi.cbSize = sizeof(CONSOLE_SCREEN_BUFFER_INFOEX);
	GetConsoleScreenBufferInfoEx(hOutput, &cbi);
	cbi.wAttributes = color;
	SetConsoleScreenBufferInfoEx(hOutput, &cbi);
}

/** @fn void OutputString(WORD x, WORD y, std::string const& s, WORD attributes)
	@brief outputs object to the console.
	@return void
*/
void OuterManager::OutputString(WORD x, WORD y, std::string const& s, WORD attributes) {
	InnerManager::OutputString(x, y, s, vector<WORD>(s.size(), attributes), currentConsoleWidth, hConsoleOutput);
}

/** @fn void SaveConsoleState()
	@brief saves the state of the console before making changes.
	@return void
*/
void OuterManager::SaveConsoleState() {
	try {
		hConsoleInput = GetInputHandle();
		hConsoleOutput = GetOutputHandle();

		// Get the old window/buffer size
		THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &originalCSBI));

		// Save the desktop
		originalBuffer.resize(size_t(originalCSBI.dwSize.X)*originalCSBI.dwSize.Y);
		originalBufferCoord = _coordinate{ 0 };
		bufferRect.Right = originalCSBI.dwSize.X - 1;
		bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
		THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, &bufferRect));

		// Save the cursor
		THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &originalCCI));
	}
	catch (XError) {
		MessageBoxA(NULL, "", "Runtime Error", MB_OK);
	}

}

/** @fn void HideCursor()
	@brief hides cursor.
	@return void
*/
void OuterManager::HideCursor() {
	InnerManager::SetCursorInfo(hConsoleOutput, false);
}

/** @fn void ResizeWindow(WORD WINDOW_WIDTH, WORD WINDOW_HEIGHT)
	@brief resizes console window.
	@return void
*/
void OuterManager::ResizeWindow(WORD WINDOW_WIDTH, WORD WINDOW_HEIGHT) {
	_rect sr{ 0 };
	InnerManager::SetWindow(hConsoleOutput, true, &sr);

	_coordinate bufferSize;
	bufferSize.X = WINDOW_WIDTH;
	bufferSize.Y = WINDOW_HEIGHT;
	InnerManager::SetBufferSize(hConsoleOutput, bufferSize);

	_buffer sbi;
	InnerManager::GetBuffer(hConsoleOutput);

	sr.Top = sr.Left = 0;
	WINDOW_WIDTH = std::min((SHORT)WINDOW_WIDTH, sbi.dwMaximumWindowSize.X);
	WINDOW_HEIGHT = std::min((SHORT)WINDOW_HEIGHT, sbi.dwMaximumWindowSize.Y);
	sr.Right = WINDOW_WIDTH - 1;
	sr.Bottom = WINDOW_HEIGHT - 1;

	InnerManager::SetWindow(hConsoleOutput, true, &sr);
	currentConsoleWidth = sr.Right - sr.Left + 1;


	//SMALL_RECT sr{ 0 };
	//THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));

	//COORD bufferSize;
	//bufferSize.X = WINDOW_WIDTH;
	//bufferSize.Y = WINDOW_HEIGHT;
	//THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, bufferSize));

	//CONSOLE_SCREEN_BUFFER_INFO sbi;
	//THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &sbi));

	//sr.Top = sr.Left = 0;
	//WINDOW_WIDTH = std::min((SHORT)WINDOW_WIDTH, sbi.dwMaximumWindowSize.X);
	//WINDOW_HEIGHT = std::min((SHORT)WINDOW_HEIGHT, sbi.dwMaximumWindowSize.Y);
	//sr.Right = WINDOW_WIDTH - 1;
	//sr.Bottom = WINDOW_HEIGHT - 1;

	//THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
	//currentConsoleWidth = sr.Right - sr.Left + 1;
}

/** @fn void RestoreConsoleWindow()
	@brief restores original console window.
	@return void
*/
void OuterManager::RestoreConsoleWindow() {
	//SMALL_RECT sr{ 0 };
	//InnerManager::SetWindow(hConsoleOutput, true, &sr);
	//InnerManager::SetBufferSize(hConsoleOutput);

	// Restore the original settings/size
	SMALL_RECT sr{ 0 };
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, originalCSBI.dwSize));
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &originalCSBI.srWindow));


	// Restore the desktop contents
	bufferRect = SMALL_RECT{ 0 };
	bufferRect.Right = originalCSBI.dwSize.X - 1;
	bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, &bufferRect));

	// Restore the cursor
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &originalCCI));
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, originalCSBI.dwCursorPosition));
}