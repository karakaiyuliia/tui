/*
file:		InnerManager.cpp
purpose:	Inner Facade class implementation
author:		Yuliia Karakai
date:		2/03/2019
*/

#include "../tuilib-dist/include/InnerManager.hpp"

VOID InnerManager::SetCursorPos(HANDLE hConsoleOutput, COORD pos) {
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, originalCSBI.dwCursorPosition));
}

VOID InnerManager::SetCursorInfo(HANDLE hConsoleOutput, bool visible) {
	auto newCCI = originalCCI;
	newCCI.bVisible = visible;
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &newCCI));
}

VOID InnerManager::WriteOutputA(HANDLE hConsoleOutput, PSMALL_RECT lpWriteRegion) {
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, &bufferRect));

}

/** @fn HANDLE GetInputHandle()
	@brief retrieves a handle to the specified standard device.
	@return HANDLE
*/
HANDLE InnerManager::GetInputHandle() {
	auto hStdin = GetStdHandle(STD_INPUT_HANDLE);

	return hStdin;
}

/** @fn HANDLE GetOutputHandle()
	@brief retrieves a handle to the specified standard device.
	@return HANDLE
*/
HANDLE InnerManager::GetOutputHandle() {
	auto hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	return hStdout;
}

/** @fn int GetKeyAscii(KEY_EVENT_RECORD const& ker)
	@brief returns integer representation of ascii character.
	@return int
*/
int InnerManager::GetKeyAscii(KEY_EVENT_RECORD const& ker) {
	return ker.uChar.AsciiChar;
}

/** @fn VOID ConfigureConsoleMode(HANDLE hStdin)
	@brief sets console mode.
	@return VOID
*/
VOID InnerManager::ConfigureConsoleMode(HANDLE hStdin) {
	DWORD oldConsoleMode;
	GetConsoleMode(hStdin, &oldConsoleMode);

	DWORD consoleMode = ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT | ENABLE_WINDOW_INPUT;
	consoleMode |= ENABLE_EXTENDED_FLAGS;	// Stop windows from taking over the mouse.

	if (!SetConsoleMode(hStdin, consoleMode)) {
		cerr << "\nERROR: could not set console mode.\n";
	}
}

VOID InnerManager::SetBufferSize(HANDLE hConsoleOutput, COORD coord) {
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, coord));
}

VOID InnerManager::SetWindow(HANDLE hConsoleOutput, BOOL bAbsolute, const SMALL_RECT* lpConsoleWindow) {
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, bAbsolute, lpConsoleWindow));
}

/** @fn VOID OutputString(WORD x, WORD y, std::string const& s, vector<WORD> const& attributes, WORD currentConsoleWidth, HANDLE hConsoleOutput)
	@brief outputs object to the console.
	@return VOID
*/
VOID InnerManager::OutputString(WORD x, WORD y, std::string const& s, vector<WORD> const& attributes, WORD currentConsoleWidth, HANDLE hConsoleOutput) {
	COORD loc{ (SHORT)x, (SHORT)y };
	DWORD nCharsWritten;
	DWORD nToWrite = DWORD(min(s.size(), std::size_t(currentConsoleWidth - x)));

	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), nToWrite, loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, attributes.data(), nToWrite, loc, &nCharsWritten));
}

/** @fn DWORD GetConsoleSize(HANDLE hConsoleOutput)
	@brief returns the size of the buffer.
	@return DWORD
*/
DWORD InnerManager::GetConsoleSize(HANDLE hConsoleOutput) {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	return consoleSize;
}

/** @fn VOID FillConsoleOutput(HANDLE hConsoleOutput, WORD color)
	@brief fills buffer cells with color.
	@return VOID
*/
VOID InnerManager::FillConsoleOutput(HANDLE hConsoleOutput, WORD color, short x, short y, DWORD consoleSize) {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);

	DWORD charsWritten;
	COORD cursorHomeCoord{ x, y };

	FillConsoleOutputCharacterA(hConsoleOutput, ' ', consoleSize, cursorHomeCoord, &charsWritten);
	FillConsoleOutputAttribute(hConsoleOutput, color, consoleSize, cursorHomeCoord, &charsWritten);
}

/** @fn VOID GetBuffer(HANDLE hConsoleOutput)
	@brief returns current buffer.
	@return VOID
*/
VOID InnerManager::GetBuffer(HANDLE hConsoleOutput) {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
}

/** @fn COORD GetMousePos(MOUSE_EVENT_RECORD const& mer)
	@brief returns mouse position in console.
	@return COORD
*/
COORD InnerManager::GetMousePos(MOUSE_EVENT_RECORD const& mer) {
	auto bufferLoc = mer.dwMousePosition;

	return bufferLoc;
}

/** @fn DWORD GetBtnState(MOUSE_EVENT_RECORD const& mer)
	@brief returns the status of the mouse buttons.
	@return DWORD
*/
DWORD InnerManager::GetBtnState(MOUSE_EVENT_RECORD const& mer) {
	auto mask = mer.dwButtonState;

	return mask;
}

/** @fn VOID WriteToConsole(HANDLE hConsoleOutput, string s, vector<WORD> attr, COORD bufferLoc)
	@brief fills buffers cell.
	@return VOID
*/
VOID InnerManager::WriteToConsole(HANDLE hConsoleOutput, string s, vector<WORD> attr, COORD bufferLoc) {
	DWORD nCharsWritten;
	WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), (DWORD)s.size(), bufferLoc, &nCharsWritten);
	WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), bufferLoc, &nCharsWritten);
}