#pragma once
/*
file:		PlayerConfigs.hpp
purpose:	configs to be used across the project
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>

COORD constexpr PLAYER1_COLOR = COORD{ 0, 0 };
COORD constexpr PLAYER2_COLOR = COORD{ 40, 0 };
WORD constexpr PLAYER_COLOR_LENGTH = 40;

COORD constexpr PLAYER1_EDIT_CONTROL_START = COORD{ 10, 10 };
COORD constexpr PLAYER2_EDIT_CONTROL_START = COORD{ 50, 10 };
WORD constexpr EDIT_CONTROL_LENGTH = 20;
WORD constexpr EDIT_CONTROL_ATTR = BACKGROUND_BLUE | FOREGROUND_WHITE;