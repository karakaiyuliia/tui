/*
file:		ListOfMoves.cpp
purpose:	ListOfMoves class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/


#include "ListOfMoves.hpp"

/** @fn void outputMove(OuterManager m, unsigned short x, unsigned short y, COORD coord, WORD color)
	@brief outputs moves information at specific position
	@return void
*/
void ListOfMoves::outputMove(OuterManager m, unsigned short x, unsigned short y, COORD coord, WORD color) {
	string outputX = "X:" + to_string(x);
	string outputY = ", Y:" + to_string(y);
	m.OutputString(coord.X, coord.Y, outputX + outputY, color);
}

/** @fn void undoMove(OuterManager m, COORD coord)
	@brief fills cell with white color if there is an undo operation
	@return void
*/
void ListOfMoves::undoMove(OuterManager m, COORD coord) {
	m.FillConsoleOutput(BACKGROUND_WHITE, coord.X, coord.Y, 40);
}