#pragma once

/*
file:		Win.hpp
purpose:	Win class declaration. Holds declaration for functions that are executed if someone wins
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include "PlayerConfigs.hpp"
#include<string>
using namespace std;

class Win {
public:
	void someoneWon(OuterManager m, string winner, short color);
};