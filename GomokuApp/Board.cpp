/*
file:		Board.cpp
purpose:	Board class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Board.hpp"

/** @fn void createBoardDividers(OuterManager m, DWORD size, WORD color, COORD coord)
	@brief immitates Gomoku board
	@return void
*/
void Board::createBoardDividers(OuterManager m, DWORD size, WORD color, COORD coord) {
	DWORD charsWritten;
	FillConsoleOutputCharacterA(m.hConsoleOutput, '|', size, coord, &charsWritten);
}