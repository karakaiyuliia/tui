#pragma once

/*
file:		Player.hpp
purpose:	Player class declaration. Holds declaration for functions that create players' controls and colours
author:		Yuliia Karakai
date:		18/04/2019
*/


#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include "PlayerConfigs.hpp"

class Player {
public: 
	void createTxtField(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2);
	void createColor(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2);
};