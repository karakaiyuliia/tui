/*
file:		Command.cpp
purpose:	Command class that holds communication between model and controller
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <iostream>
#include <conio.h>
#include <memory>
#include <map>
#include "Gomoku.hpp"

using namespace std;


class Command {
public:
	HANDLE hStdin;
	using ptr = std::shared_ptr<Command>;
	virtual void execute(INPUT_RECORD rec) = 0;
	virtual ~Command() {}
};

class MouseCommand : public Command {
	Gomoku& gomoku;
public:
	MouseCommand(Gomoku& g) : gomoku(g) {}
	//mouse moved
	void execute(INPUT_RECORD rec) override { gomoku.mouseEv(rec.Event.MouseEvent); }
};

class ButtonCommand : public Command {
	Gomoku& gomoku;
public:
	ButtonCommand(Gomoku& g) : gomoku(g) {}
	//button clicked
	void execute(INPUT_RECORD rec) override { gomoku.btnClicked(rec.Event.KeyEvent); }
};

class SaveCommand : public Command {
	Gomoku& gomoku;
public:
	SaveCommand(Gomoku& g) : gomoku(g) {}
	//save original console
	void execute(INPUT_RECORD rec) override { gomoku.saveConsole(); }
};

class ConfigureCommand : public Command {
	Gomoku& gomoku;
public:
	ConfigureCommand(Gomoku& g) : gomoku(g) {}
	//run all config functions
	void execute(INPUT_RECORD rec) override {
		gomoku.hideCursor();
		gomoku.resizeWindow(80, 40);
		gomoku.setColor(BACKGROUND_WHITE);
		gomoku.configureConsole();
		OuterManager::SetCtrlHandler();
		gomoku.createBoard(gomoku.manager);
		gomoku.showScore(gomoku.manager);
		gomoku.setPlayersColor(gomoku.manager);
		gomoku.setTxtFields(gomoku.manager);
		hStdin = gomoku.getInputHndl();

	}
};

class RestoreCommand : public Command {
	Gomoku& gomoku;
public:
	RestoreCommand(Gomoku& g) : gomoku(g) {}
	//restore original console state
	void execute(INPUT_RECORD rec) override { gomoku.restoreConsole(); }
};