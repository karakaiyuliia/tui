/*
file:		Moves.cpp
purpose:	class to be used to store move information in the controller
author:		Yuliia Karakai
date:		18/04/2019
*/


#include<Windows.h>

class Moves {
public:
	int x;
	int y;
	WORD color;
};