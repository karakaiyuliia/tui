#pragma once
/*
file:		ListOfMoves.hpp
purpose:	ListOfMoves class declaration. Holds declaration for functions that are related to list of moves
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>

class ListOfMoves {
public:
	void outputMove(OuterManager m, unsigned short x, unsigned short y, COORD coord, WORD color);
	void undoMove(OuterManager m, COORD coord);
};