/*
file:		Player.cpp
purpose:	Player class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Player.hpp"

/** @fn void createColor(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2)
	@brief fills console at specific position with black and red to specify players' colours
	@return void
*/
void Player::createColor(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2) {
	m.FillConsoleOutput(color1, coord1.X, coord1.Y, size);
	m.FillConsoleOutput(color2, coord2.X, coord2.Y, size);
}

/** @fn void createTxtField(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2)
	@brief fills console at specific position with blue color to indicate text fields areas for entering names
	@return void
*/
void Player::createTxtField(OuterManager m, DWORD size, WORD color1, WORD color2, COORD coord1, COORD coord2) {
	m.FillConsoleOutput(color1, coord1.X, coord1.Y, size);
	m.FillConsoleOutput(color2, coord2.X, coord2.Y, size);
}
