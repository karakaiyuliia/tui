#pragma once

/*
file:		Score.hpp
purpose:	Score class declaration. Holds declaration for functions show and updates user score
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include "PlayerConfigs.hpp"

class Score {
public: 
	void showScore(OuterManager m, int score1, int score2, COORD coord1, COORD coord2);
};