/*
file:		Gomoku.cpp
purpose:	Gomoku class implementation. Main application logic
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Gomoku.hpp"
#include <iostream>
#include <map>

/*variables*/
vector<unsigned short> gomokuAttr{ BACKGROUND_RED };
short redListY = 3;
short blackListY = 3;
bool allowUndo = false;
unsigned short lastMove;
int player1Score = 0;
int player2Score = 0;
bool editControl1HasFocus = false;
bool editControl2HasFocus = false;
string editControl1String;
string editControl2String;
string::size_type editControl1CursorPos = 0;
string::size_type editControl2CursorPos = 0;
decltype(editControl1CursorPos) editControl1Aperture = 0;
decltype(editControl1CursorPos) editControl2Aperture = 0;


/** @fn void createBoard(OuterManager m)
	@brief calls createBoardDividers from view to immitate Gommoku board
	@return void
*/
void Gomoku::createBoard(OuterManager m) {
	auto size = m.GetConsoleSize();
	b.createBoardDividers(m, size, BACKGROUND_BLACK, { 0, PLAYER2_EDIT_CONTROL_START.Y + 1 });
}

/** @fn void showScore(OuterManager m)
	@brief calls showScore from view to update player's score
	@return void
*/
void Gomoku::showScore(OuterManager m) {
	_coordinate player1 = { 10,2 };
	_coordinate player2 = { 50,2 };

	s.showScore(m, player1Score, player2Score, player1, player2);
}

/** @fn void setPlayersColor(OuterManager m)
	@brief calls createColor from view to set red and black colours to players
	@return void
*/
void Gomoku::setPlayersColor(OuterManager m) {
	_coordinate player1 = { 0,0 };
	_coordinate player2 = { 40, 0 };
	p.createColor(m, PLAYER_COLOR_LENGTH, BACKGROUND_RED, BACKGROUND_BLACK, PLAYER1_COLOR, PLAYER2_COLOR);
}

/** @fn void setTxtFields(OuterManager m)
	@brief calls createTxtField from view to create 2 text fields to enter user names
	@return void
*/
void Gomoku::setTxtFields(OuterManager m) {
	p.createTxtField(m, EDIT_CONTROL_LENGTH, BACKGROUND_BLUE, BACKGROUND_BLUE, PLAYER1_EDIT_CONTROL_START, PLAYER2_EDIT_CONTROL_START);

}

/** @fn _handler getInputHndl()
	@brief overriden retrieving a handle to the specified standard device.
	@return _handler
*/
Gomoku::_handler Gomoku::getInputHndl() {
	return manager.GetInputHandle();
}

/** @fn void configureConsole()
	@brief overriden console state config function.
	@return void
*/
void Gomoku::configureConsole() {
	auto hStdin = manager.GetInputHandle();
	manager.ConfigureConsoleMode(hStdin);
}

/** @fn bool row(int x, int y, vector<Moves> moves)
	@brief determines if there are 5 in a row.
	@return bool
	@note doesn't work
*/
bool Gomoku::row(int x, int y, vector<Moves> moves) {
	bool win = true;
	int i = 0;
	int found = 0;

	while (win) {
		bool exists = false;
		++i;
		for (Moves move : moves) {
			//check to the right
			if (move.x == x + i && move.y == y) {
				exists = true;
				++found;
			}
			//check to the left
			if (move.x == x - i && move.y == y) {
				exists = true;
				++found;
			}
		}

		if (!exists) {
			win = false;
			break;
		}
		if (found >= 4) {
			return true;
		}
	}

	return false;

}

/** @fn bool column(int x, int y, vector<Moves> moves)
	@brief determines if there are 5 in a column.
	@return bool
	@note doesn't work
*/
bool Gomoku::column(int x, int y, vector<Moves> moves) {
	bool win = true;
	int i = 0;
	int found = 0;

	while (win) {
		bool exists = false;
		++i;
		for (Moves move : moves) {
			//check to the right
			if (move.y == y + i && move.x == x) {
				exists = true;
				++found;
			}
			//check to the left
			if (move.y == y - i && move.x == x) {
				exists = true;
				++found;
			}
		}

		if (!exists) {
			win = false;
			break;
		}
		if (found >= 4) {
			return true;
		}
	}

	return false;
}

/** @fn bool column(int x, int y, vector<Moves> moves)
	@brief determines if there there is a win.
	@return bool
*/
bool Gomoku::win(int x, int y, vector<Moves> moves) {
	return row(x, y, moves) || column(x,y,moves);
}

/** @fn void timer(int sec)
	@brief sets timer and delays execution
	@return bool
*/
void Gomoku::timer(int sec) {
	Sleep(sec * 1000);
}

/** @fn void restart()
	@brief restarts the game and sets all controls to initial state
	@return bool
*/
void Gomoku::restart() {
	editControl1String.clear();
	editControl2String.clear();
	blackListY = 3;
	redListY = 3;
	player1Score = 0;
	player2Score = 0;
	showScore(manager);
	setPlayersColor(manager);
	manager.FillConsoleOutput(BACKGROUND_WHITE, 0, PLAYER1_EDIT_CONTROL_START.Y - 7, manager.GetConsoleSize());
	setTxtFields(manager);
	createBoard(manager);
	redMoves.clear();
	blackMoves.clear();
}

/** @fn void undo()
	@brief undo the last operation
	@return bool
*/
void Gomoku::undo() {
	_coordinate bufferLoc;
	vector<unsigned short> outAttr{BACKGROUND_WHITE};
	string block = " ";

	if (lastMove == BACKGROUND_BLACK) {
		gomokuAttr[0] = BACKGROUND_BLACK;
		bufferLoc.X = blackMoves[blackMoves.size() - 1].x;
		bufferLoc.Y = blackMoves[blackMoves.size() - 1].y;
		blackMoves.erase(blackMoves.end()-1);
		--player2Score;
		manager.OutputString(bufferLoc.X, bufferLoc.Y, "|", FOREGROUND_BLACK | BACKGROUND_WHITE);
		--blackListY;
		list.undoMove(manager, { (short)41, (short)blackListY});
		showScore(manager);

		//you can undo only once
		allowUndo = false;
	}
	else {
		gomokuAttr[0] = BACKGROUND_RED;
		bufferLoc.X = redMoves[redMoves.size() - 1].x;
		bufferLoc.Y = redMoves[redMoves.size() - 1].y;
		redMoves.erase(redMoves.end()-1);
		--player1Score;
		manager.OutputString(bufferLoc.X, bufferLoc.Y, "|", FOREGROUND_BLACK | BACKGROUND_WHITE);
		--redListY;
		list.undoMove(manager, { (short)0, (short)redListY });
		showScore(manager);

		//you can undo only once
		allowUndo = false;
	}
}

/** @fn void mouseEv(_mouse_ev mer)
	@brief handles mouse move event and determines the correct operation
	@return void
*/
void Gomoku::mouseEv(_mouse_ev mer) {
	manager.GetBuffer();
	string block = " ";
	auto bufferLoc = manager.GetMousePos(mer);
	vector<unsigned short> outAttr{};
	auto mask = manager.GetBtnState(mer);

	//NOTE: application specific Win32 functions. Separate file? Library??
	if (mask&LEFT_CLICK)
	{
		//if text field for eather player was hit
		if (bufferLoc.Y < PLAYER1_EDIT_CONTROL_START.Y + 1) {
			auto editControlHitPlayer1 = (bufferLoc.Y == PLAYER1_EDIT_CONTROL_START.Y && PLAYER1_EDIT_CONTROL_START.X <= bufferLoc.X && bufferLoc.X < PLAYER1_EDIT_CONTROL_START.X + EDIT_CONTROL_LENGTH);
			auto editControlHitPlayer2 = (bufferLoc.Y == PLAYER2_EDIT_CONTROL_START.Y && PLAYER2_EDIT_CONTROL_START.X <= bufferLoc.X && bufferLoc.X < PLAYER2_EDIT_CONTROL_START.X + EDIT_CONTROL_LENGTH);

			//player 1 was hit
			if (editControlHitPlayer1) {
				// show the cursor at the selection point
				_cursor cci{ 10, TRUE };
				THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(manager.hConsoleOutput, &cci));
				editControl1CursorPos = min(bufferLoc.X - PLAYER1_EDIT_CONTROL_START.X + editControl1Aperture, editControl1String.size());
				_coordinate loc{ short(editControl1CursorPos - editControl1Aperture + PLAYER1_EDIT_CONTROL_START.X), PLAYER1_EDIT_CONTROL_START.Y };
				THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(manager.hConsoleOutput, loc));


			}

			//player 2 was hit
			else if (editControlHitPlayer2) {
				// show the cursor at the selection point
				_cursor cci{ 10, TRUE };
				THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(manager.hConsoleOutput, &cci));
				editControl2CursorPos = min(bufferLoc.X - PLAYER2_EDIT_CONTROL_START.X + editControl2Aperture, editControl2String.size());
				_coordinate loc{ short(editControl2CursorPos - editControl2Aperture + PLAYER2_EDIT_CONTROL_START.X), PLAYER2_EDIT_CONTROL_START.Y };
				THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(manager.hConsoleOutput, loc));
			}

			//no text fields were hit, so hide the cursor 
			else if (!editControlHitPlayer1 && !editControlHitPlayer2 && editControl1HasFocus && editControl2HasFocus) {
				_cursor cci{ 10, FALSE };
				THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(manager.hConsoleOutput, &cci));
			}

			editControl1HasFocus = editControlHitPlayer1;
			editControl2HasFocus = editControlHitPlayer2;
		}

		//play area was hit
		else {
			//undo is allowed after each move
			allowUndo = true;

			//switch colours after every move
			outAttr.push_back(gomokuAttr[0]);
			if (gomokuAttr[0] == BACKGROUND_RED)
				gomokuAttr[0] = BACKGROUND_BLACK;
			else if (gomokuAttr[0] == BACKGROUND_BLACK)
				gomokuAttr[0] = BACKGROUND_RED;

			manager.WriteToConsole(block, outAttr, bufferLoc);

			Moves move;
			move.x = bufferLoc.X;
			move.y = bufferLoc.Y;
			move.color = outAttr[0];

			//save moves to corresponding vectors
			if (move.color == BACKGROUND_RED) {
				redMoves.push_back(move);
				lastMove = BACKGROUND_RED;
				++player1Score;

				//no scrolling, so output only a part of moves
				if (redListY < PLAYER2_EDIT_CONTROL_START.Y - 1) {
					list.outputMove(manager, redMoves[redMoves.size() - 1].x, redMoves[redMoves.size() - 1].y - PLAYER1_EDIT_CONTROL_START.Y - 1,{ 10,redListY }, BACKGROUND_WHITE | FOREGROUND_RED);
					++redListY;
				}

				//win condition
				if (win(redMoves[redMoves.size() - 1].x, redMoves[redMoves.size() - 1].y, redMoves)) {
					Win win;
					win.someoneWon(manager, "RED", BACKGROUND_RED);
					//automatically restart game after 3 seconds
					timer(3);
					restart();
				}
			}
			else {
				blackMoves.push_back(move);
				lastMove = BACKGROUND_BLACK;
				++player2Score;

				//no scrolling, so output only a part of moves
				if (blackListY < PLAYER2_EDIT_CONTROL_START.Y - 1) {
					list.outputMove(manager, blackMoves[blackMoves.size() - 1].x, blackMoves[blackMoves.size() - 1].y - PLAYER1_EDIT_CONTROL_START.Y - 1, { 50,blackListY }, BACKGROUND_WHITE | FOREGROUND_BLACK);
					++blackListY;
				}

				//win condition
				if (win(blackMoves[blackMoves.size() - 1].x, blackMoves[blackMoves.size() - 1].y, blackMoves)) {
					Win win;
					win.someoneWon(manager, "BLACK", BACKGROUND_BLACK);
					//automatically restart game after 3 seconds
					timer(3);
					restart();
				}
			}

			//update score view
			showScore(manager);
		}

	}

}

/** @fn void btnClicked(_key_ev ker)
	@brief clears the screen.
	@return void
*/
void Gomoku::btnClicked(_key_ev ker) {
	if (ker.bKeyDown) {

		//text field for player 1
		if (editControl1HasFocus) {
			switch (ker.wVirtualKeyCode) {
				//set behaviour in text fields (left, right, delete etc)
			case VK_BACK:
				// backspace to remove at cursor location
				if (0 < editControl1CursorPos && editControl1CursorPos <= editControl1String.size()) {
					--editControl1CursorPos;
					editControl1String.erase(editControl1CursorPos, 1);
				}
				break;

			case VK_DELETE:
				if (0 <= editControl1CursorPos && editControl1CursorPos < editControl1String.size())
					editControl1String.erase(editControl1CursorPos, 1);
				break;

			case VK_LEFT:
				if (editControl1CursorPos > 0)
					--editControl1CursorPos;
				break;

			case VK_RIGHT:
				if (editControl1CursorPos < editControl1String.size())
					++editControl1CursorPos;
				break;

			case VK_END:
				editControl1CursorPos = editControl1String.size();
				break;

			case VK_HOME:
				editControl1CursorPos = 0;
				break;

			//if Enter detected,update player names 
			case VK_RETURN:
				manager.OutputString(PLAYER1_COLOR.X + 10, 0, editControl1String, FOREGROUND_WHITE | BACKGROUND_RED);
				editControl1HasFocus = false;
				break;

			default:
				// add character to the text field
				char ch = ker.uChar.AsciiChar;
				if (isprint(ch))
					editControl1String.insert(editControl1CursorPos++ + editControl1String.begin(), ch);
			}

			// show the string in the control
			auto practicalSize = editControl1String.size() + 1;
			while (editControl1CursorPos < editControl1Aperture)
				--editControl1Aperture;

			while (editControl1CursorPos - editControl1Aperture >= EDIT_CONTROL_LENGTH)
				++editControl1Aperture;

			while (practicalSize - editControl1Aperture<EDIT_CONTROL_LENGTH && practicalSize > EDIT_CONTROL_LENGTH)
				--editControl1Aperture;

			auto s = editControl1String.substr(editControl1Aperture, EDIT_CONTROL_LENGTH);
			s += string(EDIT_CONTROL_LENGTH - s.size(), ' ');

			manager.OutputString(PLAYER1_EDIT_CONTROL_START.X, PLAYER1_EDIT_CONTROL_START.Y, s, EDIT_CONTROL_ATTR);

			// place cursor in the control
			_coordinate cursorLoc = PLAYER1_EDIT_CONTROL_START;
			cursorLoc.X += short(editControl1CursorPos - editControl1Aperture);
			SetConsoleCursorPosition(manager.hConsoleOutput, cursorLoc);

		}

		//text field for player 2
		else if (editControl2HasFocus) {
			switch (ker.wVirtualKeyCode) {
			case VK_BACK:
				// backspace to remove at cursor location
				if (0 < editControl2CursorPos && editControl2CursorPos <= editControl2String.size()) {
					--editControl2CursorPos;
					editControl2String.erase(editControl2CursorPos, 1);
				}
				break;

			case VK_DELETE:
				if (0 <= editControl2CursorPos && editControl2CursorPos < editControl2String.size())
					editControl2String.erase(editControl2CursorPos, 1);
				break;

			case VK_LEFT:
				if (editControl2CursorPos > 0)
					--editControl2CursorPos;
				break;

			case VK_RIGHT:
				if (editControl2CursorPos < editControl2String.size())
					++editControl2CursorPos;
				break;

			case VK_END:
				editControl2CursorPos = editControl2String.size();
				break;

			case VK_HOME:
				editControl2CursorPos = 0;
				break;

			case VK_RETURN:
				manager.OutputString(PLAYER2_COLOR.X + 10, 0, editControl2String, FOREGROUND_WHITE | BACKGROUND_BLACK);
				editControl2HasFocus = false;
				break;

			default:
				// add character to the text field
				char ch = ker.uChar.AsciiChar;
				if (isprint(ch))
					editControl2String.insert(editControl2CursorPos++ + editControl2String.begin(), ch);
			}

			// show the string in the control
			auto practicalSize = editControl2String.size() + 1;
			while (editControl2CursorPos < editControl2Aperture)
				--editControl2Aperture;

			while (editControl2CursorPos - editControl2Aperture >= EDIT_CONTROL_LENGTH)
				++editControl2Aperture;

			while (practicalSize - editControl2Aperture<EDIT_CONTROL_LENGTH && practicalSize > EDIT_CONTROL_LENGTH)
				--editControl2Aperture;

			auto s = editControl2String.substr(editControl2Aperture, EDIT_CONTROL_LENGTH);
			s += string(EDIT_CONTROL_LENGTH - s.size(), ' ');

			manager.OutputString(PLAYER2_EDIT_CONTROL_START.X, PLAYER2_EDIT_CONTROL_START.Y, s, EDIT_CONTROL_ATTR);

			// place cursor in the control
			_coordinate cursorLoc = PLAYER2_EDIT_CONTROL_START;
			cursorLoc.X += short(editControl2CursorPos - editControl2Aperture);
			SetConsoleCursorPosition(manager.hConsoleOutput, cursorLoc);

		}
		//if 'r' detected, restart
		else if (manager.GetKeyAscii(ker) == 114) {
			restart();
		}
		//uf 'z' detected, undo last move
		else if (manager.GetKeyAscii(ker) == 122 && allowUndo == true) {
			undo();
		}
	}
}

/** @fn void saveConsole()
	@brief overriden console saving function.
	@return void
*/
void Gomoku::saveConsole() {
	manager.SaveConsoleState();
}

/** @fn void resizeWindow(int width, int height)
	@brief overriden console resizing function.
	@return void
*/
void Gomoku::resizeWindow(int width, int height) {
	manager.ResizeWindow(width, height);
}

/** @fn void restoreConsole()
	@brief overriden console restoring function.
	@return void
*/
void Gomoku::restoreConsole() {
	manager.RestoreConsoleWindow();
}

/** @fn void setColor(unsigned short color)
	@brief overriden console background set function.
	@return void
*/
void Gomoku::setColor(unsigned short color) {
	manager.SetConsoleBackgroundColor(color);
}

/** @fn void hideCursor()
	@brief overriden console hiding cursor function.
	@return void
*/
void Gomoku::hideCursor() {
	manager.HideCursor();
}