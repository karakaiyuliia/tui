/*
file:		Score.cpp
purpose:	Score class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Score.hpp"

/** @fn void showScore(OuterManager m, int score1, int score2, COORD coord1, COORD coord2)
	@brief outputs player's score after each move
	@return void
*/
void Score::showScore(OuterManager m, int score1, int score2, COORD coord1, COORD coord2) {
	m.OutputString(coord1.X, coord1.Y, "Pieces played: " + to_string(score1), BACKGROUND_WHITE | FOREGROUND_RED);
	m.OutputString(coord2.X, coord2.Y, "Pieces played: " + to_string(score2), BACKGROUND_WHITE | FOREGROUND_BLACK);
}