/*
file:		Win.cpp
purpose:	Win class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Win.hpp"

/** @fn void someoneWon(OuterManager m, string winner, short color)
	@brief functionality if win condition is true
	@return void
*/
void Win::someoneWon(OuterManager m, string winner, short color) {
	DWORD charsWritten;
	FillConsoleOutputAttribute(m.hConsoleOutput, color, 10, { 30,20 }, &charsWritten);
	m.OutputString(30, 20, winner + " won!", FOREGROUND_WHITE | color);
}