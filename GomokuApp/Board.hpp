#pragma once

/*
file:		Board.hpp
purpose:	Board class declaration. Holds declaration for board creation function
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>


class Board {
public:
	void createBoardDividers(OuterManager m, DWORD size, WORD color, COORD coord);
};