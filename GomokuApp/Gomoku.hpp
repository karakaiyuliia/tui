#pragma once

/*
file:		Gomoku.hpp
purpose:	Gomoku class declaration. Holds declaration for gomoku functionality methods
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include "Player.hpp"
#include <vector> 
#include "PlayerConfigs.hpp"
#include "Win.hpp"
#include "Board.hpp"
#include "Score.hpp"
#include "ListOfMoves.hpp"
#include "Moves.cpp"

using namespace std;

class Gomoku : OuterManager {
public:
	Player p;
	Board b;
	Score s;
	ListOfMoves list;
	OuterManager manager;
	vector<Moves> redMoves;
	vector<Moves> blackMoves;

	bool row(int x, int y, vector<Moves> moves);
	bool column(int x, int y, vector<Moves> moves);
	bool win(int x, int y, vector<Moves> moves);
	void createBoard(OuterManager m);
	void showScore(OuterManager m);
	void mouseEv(_mouse_ev mer);
	void btnClicked(_key_ev ker);
	void saveConsole();
	void restoreConsole();
	void resizeWindow(int width, int height);
	void configureConsole();
	void hideCursor();
	void setColor(unsigned short color);
	void setPlayersColor(OuterManager m);
	void setTxtFields(OuterManager m);
	void timer(int sec);
	void restart();
	void undo();
	_handler getInputHndl();
};