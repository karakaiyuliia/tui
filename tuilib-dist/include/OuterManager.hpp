#pragma once

/*
file:		OuterManager.hpp
purpose:	Outer Facade class declaration. Holds declaration for methods that wrap Inner Facade and add enhanecements 
author:		Yuliia Karakai
date:		2/03/2019
*/

#include "InnerManager.hpp"
#undef min
#include <algorithm>
#include "XError.hpp"

class OuterManager : protected InnerManager {
public:
	// System Data
	_handler							hConsoleInput, hConsoleOutput;


	static void CtrlHandler(DWORD ctrlType);
	static void SetCtrlHandler();
	_handler GetInputHandle();
	 _handler GetOutputHandle();
	int GetKeyAscii(_key_ev const& ker);
	unsigned long GetConsoleSize();
	void FillConsoleOutput(unsigned short color, short x, short y, unsigned long consoleSize);
	void ConfigureConsoleMode(_handler hStdin);
	void GetBuffer();
	_coordinate GetMousePos(_mouse_ev const& mer);
	unsigned long GetBtnState(_mouse_ev const& mer);
	void WriteToConsole(string s, vector<WORD> attr, COORD bufferLoc);
	void SetConsoleBackgroundColor(WORD color);
	void SaveConsoleState();
	void HideCursor();
	void ResizeWindow(WORD WINDOW_WIDTH, WORD WINDOW_HEIGHT);
	void RestoreConsoleWindow();
	void OutputString(WORD x, WORD y, std::string const& s, WORD attributes);
};
