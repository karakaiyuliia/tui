#pragma once

/*
file:		config.hpp
purpose:	configuration file
author:		Yuliia Karakai
date:		2/03/2019
*/

#if defined(_DEBUG) && defined(_DLL)
#pragma comment (lib, "tuilib-mt-gd.lib")
#elif defined(_DEBUG) && !defined(_DLL)
#pragma comment (lib, "tuilib-mt-sgd.lib")
#elif !defined(_DEBUG) && defined(_DLL)
#pragma comment (lib, "tuilib-mt.lib")
#elif !defined(_DEBUG) && !defined(_DLL)
#pragma comment (lib, "tuilib-mt-s.lib")
#endif