#pragma once

/*
file:		InnerManager.hpp
purpose:	Inner Facade class declaration. Holds declaration for methods that wrap Win32 API
author:		Yuliia Karakai
date:		2/03/2019
*/

#undef min
#include "XError.hpp"
#include <algorithm>
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Setup DBG environment
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>

// Create a dbg version of new that provides more information
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
#define DBG_NEW new
#endif

//define macros to use across application
#define MOUSE       0x0002
#define KEY         0x0001
#define BACKGROUND_BLACK       0
#define BACKGROUND_WHITE		BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE
#define FOREGROUND_WHITE		FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE
#define FOREGROUND_BLACK		0
#define LEFT_CLICK  FROM_LEFT_1ST_BUTTON_PRESSED
#define RIGHT_CLICK RIGHTMOST_BUTTON_PRESSED
#define READ_INPUT	ReadConsoleInput
#define INPUT		_INPUT_RECORD

class InnerManager {
public:
	//simplifying Win32 API data types
	using _mouse_ev = MOUSE_EVENT_RECORD;
	using _key_ev = KEY_EVENT_RECORD;
	using _handler = HANDLE;
	using _coordinate = COORD;
	using _buffer = CONSOLE_SCREEN_BUFFER_INFO;
	using _cursor = CONSOLE_CURSOR_INFO;
	using _char = CHAR_INFO;
	using _rect = SMALL_RECT;

	_buffer								originalCSBI;
	_cursor								originalCCI;
	vector<_char>						originalBuffer;
	_rect bufferRect{ 0 };
	_coordinate							originalBufferCoord;
	unsigned long						originalConsoleMode;
	unsigned short						currentConsoleWidth = 0;


protected:
	HANDLE GetInputHandle();
	HANDLE GetOutputHandle();
	int GetKeyAscii(KEY_EVENT_RECORD const& ker);
	VOID ConfigureConsoleMode(HANDLE hStdin);
	DWORD GetConsoleSize(HANDLE hConsoleOutput);
	VOID FillConsoleOutput(HANDLE hConsoleOutput, WORD color, short x, short y, DWORD consoleSize);
	VOID GetBuffer(HANDLE hConsoleOutput);
	COORD GetMousePos(MOUSE_EVENT_RECORD const& mer);
	DWORD GetBtnState(MOUSE_EVENT_RECORD const& mer);
	VOID WriteToConsole(HANDLE hConsoleOutput, string s, vector<WORD> attr, COORD bufferLoc);
	VOID OutputString(WORD x, WORD y, std::string const& s, vector<WORD> const& attributes, WORD currentConsoleWidth, HANDLE hConsoleOutput);
	VOID SetCursorPos(HANDLE hConsoleOutput, COORD pos);
	VOID SetCursorInfo(HANDLE hConsoleOutput,  bool visible);
	VOID SetWindow(HANDLE hConsoleOutput, BOOL bAbsolute, const SMALL_RECT* lpConsoleWindow);
	VOID SetBufferSize(HANDLE hConsoleOutput, COORD coord);
	VOID WriteOutputA(HANDLE hConsoleOutput, PSMALL_RECT lpWriteRegion);
};