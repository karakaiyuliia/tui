#pragma once

/*
file:		Palette.hpp
purpose:	Palette class declaration. Holds declaration for palette functionality methods
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <Windows.h>
#include <..\..\tuilib-dist\include\OuterManager.hpp>


class Palette{
public:
	void createBtn(OuterManager m, short x, short y, string s, WORD attr);
};
