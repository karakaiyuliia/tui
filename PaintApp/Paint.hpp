#pragma once

/*
file:		Paint.hpp
purpose:	Paint class declaration. Holds declaration for paint functionality methods
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include "ColorConfigs.hpp"
#include "Palette.hpp"


using namespace std;


class Paint : OuterManager {
public:
	Palette p;
	OuterManager manager;
	void paint(_mouse_ev mer);
	void clearScreen(_key_ev ker);
	void saveConsole();
	void restoreConsole();
	void resizeWindow(int width, int height);
	void configureConsole();
	void hideCursor();
	void setColor(unsigned short color);
	void createBtn(OuterManager m);
	_handler getInputHndl();
};