/*
file:		Palette.cpp
purpose:	Palette class implementation
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <iostream>
#include <..\..\tuilib-dist\include\OuterManager.hpp>
#include"Palette.hpp"
using namespace std;

/** @fn void createBtn(OuterManager m, short x, short y, string s, WORD attr)
	@brief creates colour button
	@return void
*/
void Palette::createBtn(OuterManager m, short x, short y, string s, WORD attr) {
	m.OutputString(x, y, s,attr);
}