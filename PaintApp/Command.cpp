/*
file:		Command.cpp
purpose:	Command class that holds communication between model and controller
author:		Yuliia Karakai
date:		18/04/2019
*/

#include <iostream>
#include <conio.h>
#include <memory>
#include <map>
#include "Paint.hpp"
using namespace std;


class Command {
public:
	HANDLE hStdin;
	using ptr = std::shared_ptr<Command>;
	virtual void execute(INPUT_RECORD rec) = 0;
	virtual ~Command() {}
};

class MouseCommand : public Command {
	Paint& paint;
public:
	MouseCommand(Paint& p) : paint(p) {}
	//execute paint function after mouse move detected
	void execute(INPUT_RECORD rec) override { paint.paint(rec.Event.MouseEvent); }
};

class ButtonCommand : public Command {
	Paint& paint;
public:
	ButtonCommand(Paint& p) : paint(p) {}
	//execute clearScreen after button has been pressed
	void execute(INPUT_RECORD rec) override { paint.clearScreen(rec.Event.KeyEvent); }
};

class SaveCommand : public Command {
	Paint& paint;
public:
	SaveCommand(Paint& p) : paint(p) {}
	//execute function that saves original console setings
	void execute(INPUT_RECORD rec) override { paint.saveConsole(); }
};

class ConfigureCommand : public Command {
	Paint& paint;
public:
	ConfigureCommand(Paint& p) : paint(p) {}
	//execute all configs functions for paint
	void execute(INPUT_RECORD rec) override { 
		paint.hideCursor();
		paint.resizeWindow(40, 40);
		paint.setColor(BACKGROUND_WHITE);
		paint.configureConsole();
		OuterManager::SetCtrlHandler();
		paint.createBtn(paint.manager);
		hStdin = paint.getInputHndl();
	}
};

class RestoreCommand : public Command {
	Paint& paint;
public:
	RestoreCommand(Paint& p) : paint(p) {}
	//execute restoring console function
	void execute(INPUT_RECORD rec) override { paint.restoreConsole(); }
};