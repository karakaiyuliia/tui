/*
file:		Paint.cpp
purpose:	Paint class implementation. Project logic. 
author:		Yuliia Karakai
date:		18/04/2019
*/

#include "Paint.hpp"
#include <iostream>
using namespace std;

/** @fn void createBtn(OuterManager m)
	@brief calls view's createBtn function several times to create a palette
	@return void
*/
void Paint::createBtn(OuterManager m) {
	
	string s = "  ";
	p.createBtn(m, RED_BUTTON_CONTROL_START.X, RED_BUTTON_CONTROL_START.Y, s, RED_BUTTON_CONTROL_ATTRIBUTE);
	p.createBtn(m, GREEN_BUTTON_CONTROL_START.X, GREEN_BUTTON_CONTROL_START.Y, s, GREEN_BUTTON_CONTROL_ATTRIBUTE);
	p.createBtn(m, BLUE_BUTTON_CONTROL_START.X, BLUE_BUTTON_CONTROL_START.Y, s, BLUE_BUTTON_CONTROL_ATTRIBUTE);
	p.createBtn(m, CYAN_BUTTON_CONTROL_START.X, CYAN_BUTTON_CONTROL_START.Y, s, CYAN_BUTTON_CONTROL_ATTRIBUTE);
	p.createBtn(m, YELLOW_BUTTON_CONTROL_START.X, YELLOW_BUTTON_CONTROL_START.Y, s, YELLOW_BUTTON_CONTROL_ATTRIBUTE);
	p.createBtn(m, MAGENTA_BUTTON_CONTROL_START.X, MAGENTA_BUTTON_CONTROL_START.Y, s, MAGENTA_BUTTON_CONTROL_ATTRIBUTE);
}

/** @fn _handler getInputHndl()
	@brief overriden retrieving a handle to the specified standard device.
	@return _handler
*/
Paint::_handler Paint::getInputHndl() {
	return manager.GetInputHandle();
}

/** @fn void configureConsole()
	@brief overriden console state config function.
	@return void
*/
void Paint::configureConsole() {
	auto hStdin = manager.GetInputHandle();
	manager.ConfigureConsoleMode(hStdin);
}

/** @fn void paint(_mouse_ev mer)
	@brief draws lines and changes colors.
	@return void
*/
vector<unsigned short> paintAttr{ BACKGROUND_RED };
void Paint::paint(_mouse_ev mer) {
	auto hStdin = manager.GetInputHandle();
	auto hStdout = manager.GetOutputHandle();

	Paint::configureConsole();

	// Get the number of character cells in the current buffer
	manager.GetBuffer();
	string draw = " ";
	auto bufferLoc = manager.GetMousePos(mer);
	vector<unsigned short> outAttr{};
	auto mask = manager.GetBtnState(mer);

	if (mask&LEFT_CLICK)
	{
		outAttr.push_back(paintAttr[0]);
		auto buttonControlHit = bufferLoc.X >= RED_BUTTON_CONTROL_START.X && bufferLoc.Y == RED_BUTTON_CONTROL_START.Y && bufferLoc.X <= MAGENTA_BUTTON_CONTROL_START.X;

		//if palette clicked
		if (buttonControlHit) {
			//looking for specific button in palette
			if (bufferLoc.X == RED_BUTTON_CONTROL_START.X && bufferLoc.X <= RED_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y == RED_BUTTON_CONTROL_START.Y && bufferLoc.Y <= RED_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_RED;
			else if (bufferLoc.X == GREEN_BUTTON_CONTROL_START.X && bufferLoc.X <= GREEN_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y == GREEN_BUTTON_CONTROL_START.Y && bufferLoc.Y <= GREEN_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_GREEN;
			else if (bufferLoc.X == BLUE_BUTTON_CONTROL_START.X && bufferLoc.X <= BLUE_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y == BLUE_BUTTON_CONTROL_START.Y && bufferLoc.Y <= BLUE_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_BLUE;			
			else if (bufferLoc.X >= MAGENTA_BUTTON_CONTROL_START.X && bufferLoc.X <= MAGENTA_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y >= MAGENTA_BUTTON_CONTROL_START.Y && bufferLoc.Y <= MAGENTA_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_MAGENTA;			
			else if (bufferLoc.X == YELLOW_BUTTON_CONTROL_START.X && bufferLoc.X <= YELLOW_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y == YELLOW_BUTTON_CONTROL_START.Y && bufferLoc.Y <= YELLOW_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_YELLOW;			
			else if (bufferLoc.X == CYAN_BUTTON_CONTROL_START.X && bufferLoc.X <= CYAN_BUTTON_CONTROL_START.X + 2 && bufferLoc.Y == CYAN_BUTTON_CONTROL_START.Y && bufferLoc.Y <= CYAN_BUTTON_CONTROL_START.Y + 1)
				paintAttr[0] = BACKGROUND_CYAN;
		}
	}

	//prevent drawing in palette area
	if(bufferLoc.Y > 0)
		manager.WriteToConsole(draw, outAttr, bufferLoc);
}

/** @fn void clearScreen(_key_ev ker)
	@brief clears the screen.
	@return void
*/
void Paint::clearScreen(_key_ev ker) {
	if (manager.GetKeyAscii(ker) == 99) {
		manager.FillConsoleOutput(BACKGROUND_WHITE, 0, 1, manager.GetConsoleSize());
	}
}


/** @fn void saveConsole()
	@brief overriden console saving function.
	@return void
*/
void Paint::saveConsole() {
	manager.SaveConsoleState();
}

/** @fn void resizeWindow(int width, int height)
	@brief overriden console resizing function.
	@return void
*/
void Paint::resizeWindow(int width, int height) {
	manager.ResizeWindow(width, height);
}

/** @fn void restoreConsole()
	@brief overriden console restoring function.
	@return void
*/
void Paint::restoreConsole() {
	manager.RestoreConsoleWindow();
}

/** @fn void setColor(unsigned short color)
	@brief overriden console background set function.
	@return void
*/
void Paint::setColor(unsigned short color) {
	manager.SetConsoleBackgroundColor(color);
}

/** @fn void hideCursor()
	@brief overriden console hiding cursor function.
	@return void
*/
void Paint::hideCursor() {
	manager.HideCursor();
}