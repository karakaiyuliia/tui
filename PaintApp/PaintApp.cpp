/*
file:		PaintApp.cpp
purpose:	main function. Actual application
author:		Yuliia Karakai
date:		2/03/2019
*/

#include "Command.cpp"
#include <iostream>
using namespace std;


int main() {
#ifdef _DEBUG
	// Enable CRT memory leak checking.
	int dbgFlags = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	dbgFlags |= _CRTDBG_CHECK_ALWAYS_DF;
	dbgFlags |= _CRTDBG_DELAY_FREE_MEM_DF;
	dbgFlags |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(dbgFlags);
#endif
	Paint p;
	vector<INPUT> inBuffer(128);
	bool done = false;

	//commands
	auto save = Command::ptr(new SaveCommand(p));
	auto mouse = Command::ptr(new MouseCommand(p));
	auto button = Command::ptr(new ButtonCommand(p));
	auto configure = Command::ptr(new ConfigureCommand(p));
	auto restore = Command::ptr(new RestoreCommand(p));

	save->execute(inBuffer[0]);
	configure->execute(inBuffer[0]);

	while (!done) {
		unsigned long numEvents;
		if (!READ_INPUT(configure->hStdin, inBuffer.data(), (unsigned long)inBuffer.size(), &numEvents)) {
			cerr << "Failed to read console input\n";
			break;
		}

		for (size_t iEvent = 0; iEvent < numEvents; ++iEvent) {
			switch (inBuffer[iEvent].EventType) {
			case MOUSE:
				mouse->execute(inBuffer[iEvent]);
				break;
			case KEY:
				button->execute(inBuffer[iEvent]);
				break;
			}

		}
	}
	restore->execute(inBuffer[0]);
}

